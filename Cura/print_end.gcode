G91 ;Change to relative positioning mode for filament retraction and nozzle lifting
G1 F200 E-6;Retract the filament a bit before lifting the nozzle
G1 F1000 Z5;Lift nozzle 5mm
G90 ;Change to absolute positioning mode to prepare for part rermoval
G1 X0 Y400 ;Move the print to max y pos for part rermoval
M104 S0 ; Turn off hotend
M106 S0 ; Turn off cooling fan
M140 S0 ; Turn off bed
M84 ; Disable motors

M300 S440 P200 ; Make Print Completed Tones
M300 S660 P250
M300 S880 P300