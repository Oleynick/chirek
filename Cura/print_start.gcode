; --- Homing --- ;
G1 F1000 Z5 ; Lift the nozzle 5mm before homing axes
G90 ; Absolute positioning
M82 ; Set extruder to absolute mode too
G28 ; Home all axis

; --- Parallel Heatup --- ;
M107 51 ; Start with the fan 20% to not melt duct
M140 S{material_bed_temperature_layer_0/2} ; start preheating the bed
M104 S{material_print_temperature_layer_0} ; start preheating hotend
M190 S{material_bed_temperature_layer_0} ; heat to Cura Bed setting
M109 S{material_print_temperature_layer_0} ; heat to Cura Hotend setting

; --- Initial Retraction --- ;
G21 ; Set units to millimeters
G91 ; Change to relative positioning mode for retract filament and nozzle lifting
G1 F200 E-1 ; Retract 1mm filament for a clean start
G92 E0 ; Zero the extruded length

; --- Nozzle Wipe --- ;
G90 ; Absolute positioning
G0 Z10 F1000 ; Move up to travel safely
G0 X415 Y0 F5000 ; Go to start position
G0 Z1 F1000 ; Lower to position
G0 F5000 ; Set speed for cleaning

M808 L5 ; Repeat cleaning path 5 times
G0 X416 Y0
G0 X408 Y8
G0 X416 Y16
G0 X408 Y24
G0 X416 Y24
G0 X408 Y16
G0 X416 Y8
G0 X408 Y0
M808

; --- Bed leveling --- ;
G29

G0 Z5 F1000 ; Lift up nozzle

; --- Print Start Tone --- ;
M300 S1000 P500 ; chirp to indicate starting to print